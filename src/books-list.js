import { LitElement, html, css } from 'lit-element';

class BooksList extends LitElement {

    static get styles() {
        return css`
      :host {
        display: block;
      }
    `;
    }

    static get properties() {
        return {
            data: { type: Object }

        };
    }

    constructor() {
        super();
        this.data = { "books": [] };
        this.cargarDatos();

    }

    render() {
        return html`
        aqui
        <div>
            ${this.data.books.map(v => html`${v.titulo} - ${v.autor} `)}
        </div>
      
    `;
    }

    cargarDatos() {
        console.log("Entra");
        fetch("http://localhost:3393/books/").then(response => {
            console.log("response: ", response);
            if (!response.ok) {
                throw response;
            }
            return response.json();
        }).then(data => {
            this.data = data;
            console.log("data ", data);
        }).catch(error => {
            alert("problemas con fetch: ", error);
        })
    };
}

customElements.define('books-list', BooksList);