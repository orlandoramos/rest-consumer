import { LitElement, html, css } from 'lit-element';

class TestFetch extends LitElement {

    static get styles() {
        return css`
      :host {
        display: block;
      }
    `;
    }

    static get properties() {
        return {
            planets: { type: Object }
        };
    }

    constructor() {
        super();
        this.planets = { results: [] }
    }

    render() {
        return html`
        ${this.planets.results.map((planet) => {
            return html `<div>planet: ${planet.name} rotation: ${planet.rotation_period}</div>`
        })}
    `;
    }

    cargarPlanetas() {
        fetch("http://swapi.dev/api/planets/").then(response => {
            console.log("response: ", response);
            if (!response.ok) {
                throw response;
            }
            return response.json();
        }).then(data =>{
            this.planets = data;
            console.log("data ",data);
        }).catch(error =>{
            alert.log("problemas con fetch: " , error);
        })
    };
    connectedCallback() {
        super.connectedCallback();
        try {
            this.cargarPlanetas();
        } catch (e) {
            alert.log(" error ", e);
        }
    }
}

customElements.define('test-fetch', TestFetch);