Feature: Cargar lista de productos
    Scenario: Cargar lista de productos
        When we request the products list
        Then we should receive
        | nombre | descripcion |
        | Movil XL | Telefono con una de las mejores pantallas |
        | Movil Mini | Telefono con una de las mejores camaras |
        | Movil Standard | Telefono estandar, nada especial |