const { When, Then, After } = require('@cucumber/cucumber');
const assert = require('assert');
const { Builder, By, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromedriver = require('chromedriver');

When('we request the products list', async function () {
    chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());
    this.driver = new Builder()
        .forBrowser('chrome')
        .build();

    this.driver.wait(until.elementLocated(By.className('products-list')));

    await this.driver.get('http://localhost:8000/');
})

Then('we should receive', async function (dataTable) {
    var productElements = await this.driver.findElements(By.className('producto'));
    var expectations = dataTable.hashes();
    console.log("hashes : " + dataTable);
    for (let i = 0; i < expectations.length; i++) {
        const productName = await productElements[i].findElement(By.tagName('h3')).getText();

        console.log(i + " product : " + productName);
        console.log(i + " expectation : " + expectations[i].nombre);

        assert.strictEqual(productName, expectations[i].nombre);

        const productDesc = await productElements[i].findElement(By.tagName('p')).getText();

        // console.log(i+ " productDesc : " + productDesc);
        // console.log(i+ " expectation : " +  expectations[i].descripcion);

        //assert.strictEqual(productDesc, expectations[i].descripcion);
    }
})

After(async function () {
    this.driver.close();
})