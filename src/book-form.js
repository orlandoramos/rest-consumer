import { LitElement, html, css } from 'lit-element';

class BookForm extends LitElement {

    static get styles() {
        return css`
      :host {
        display: block;
      }
    `;
    }

    static get properties() {
        return {
            autor: {type: String},
            id: {type: Number},
            titulo: {type: String}
        };
    }

    constructor() {
        super();
        this.id = 0;
        this.autor = "";
        this.titulo = "";
    }

    render() {
        return html`
     <div>
        <label for="iid">ID</label>
        <input for="iid" type="number" .value="${this.id}" @input="${this.updateId}" />
        <br />
        
        <label for="ititulo">Titulo</label>
        <input for="ititulo" type="text" .value="${this.titulo}" @input="${this.updateTitulo}" />
        <br />

        <label for="iautor">Autor</label>
        <input for="iautor" type="text" .value="${this.autor}" @input="${this.updateAutor}" />
        <br />
        <button @click="${this.buscarBook}">Buscar</button>
        <button @click="${this.crearBook}">Crear</button>
        <button @click="${this.modificarBook}">Modificar</button>
        <button @click="${this.eliminarBook}">Eliminar</button>
    </div>
    `;
    }

    getCurrentBook() {
        var book = {};
        book.id = this.id;
        book.titulo = this.titulo;
        book.autor = this.autor;
        return book;
    }

    eliminarBook() {
        const options = {
            method: 'DELETE',
            body: "",
            headers: { "Content-Type": "application/json" }
        };

        console.log("Entra");
        fetch("http://localhost:3393/books/" + this.id, options).then(response => {
            console.log("response: ", response);
            if (!response.ok) {
                throw response;
            }
            return response.json();
        }).then(data => {
            console.log("Eliminado");
        }).catch(error => {
            alert.log("problemas con fetch: ", error);
        })
    }
    modificarBook() {
        var book = this.getCurrentBook();
        const options = {
            method: 'PUT',
            body: JSON.stringify(book),
            headers: { "Content-Type": "application/json" }
        };
        fetch("http://localhost:3393/books/", options).then(response => {
            console.log("response: ", response);
            if (!response.ok) {
                throw response;
            }
            return response.json();
        }).then(data => {
            console.log("Book modifciado ");
        }).catch(error => {
            alert.log("problemas con fetch: ", error);
        })

    }
    crearBook() {
        var book = this.getCurrentBook();
        const options = {
            method: 'POST',
            body: JSON.stringify(book),
            headers: { "Content-Type": "application/json" }
        };

        fetch("http://localhost:3393/books/", options).then(response => {
            console.log("response: ", response);
            if (!response.ok) {
                throw response;
            }
            return response.json();
        }).then(data => {
            console.log("Book creado ");
        }).catch(error => {
            alert.log("problemas con fetch: ", error);
        })

    }

    buscarBook2() {
        fetch("http://localhost:3393/books/" + this.id).then(response => {
            if (!response.ok) {
                throw response;
            }
            return response.json();
        }).then((data) => {
            this.id = data.id;
            this.titulo = data.titulo;
            this.autor = data.autor;
            console.log("data ", data);
        }).catch(error => {
            alert.log("problemas con fetch: ", error);
        })
    };

    buscarBook() {
        fetch("http://localhost:3393/books/" + this.id)
            .then((response) => {
                if (!response.ok) { throw response; }
                return response.json();
            })
            .then((data) => {
                this.id = data.id;
                this.titulo = data.titulo;
                this.autor = data.autor;
            })
            .catch((e) => {
                console.log("Problemas con el fetch: e" + e);
            });
    }

    updateId(e) {
        this.id = e.target.value;
    }


    updateTitulo(e) {
        this.titulo = e.target.value;
    }

    updateAutor(e) {
        this.autor = e.target.value;
    }



}

customElements.define('book-form', BookForm);