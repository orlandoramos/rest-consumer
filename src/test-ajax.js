import { LitElement, html, css } from 'lit-element';

class TestAjax extends LitElement {

    static get styles() {
        return css`
      :host {
        display: block;
      }
    `;
    }

    static get properties() {
        return {
            planet: { type: Object }
        };
    }

    constructor() {
        super();
        this.cargarPlaneta();
        this.planet = {};
    }

    render() {
        return html`
        <p> ${this.planet.name} </p>
        <p> ${this.planet.diameter} </p>
    `;
    }

    cargarPlaneta(){
        $.get("http://swapi.dev/api/planets/1", ((data, status) =>{
            if(status === "success"){
                this.planet = data;
            }
            else{
                alert("error en llamado REST");
            }
        }));
    }
}

customElements.define('test-ajax', TestAjax);