import { LitElement, html, css } from 'lit-element';

class TestOpentable extends LitElement {

    static get styles() {
        return css`
      :host {
        display: block;
      }
      td{
          text-align: center;
      }
      button{
          float: left;
      }
    `;
    }

    static get properties() {
        return {
            resultados: { type: Object },
            campos: { type: Array },
            filas: { type: Array },
            sig: { type: Boolean },
            prev: { type: Boolean },
            entidades: { type: Array}
        };
    }

    constructor() {
        super();
        this.campos = [];
        this.resultados = { results: [] };
        this.filas = [];
        this.cargarDatos("http://swapi.dev/api/planets/");
        this.entidades = ["planets", "films", "people", "species", "starships", "vehicles"];
    }

    render() {
        return html`
        <select id = "sel" @change="${this.changeSelect}">
        ${this.entidades.map(e=> html`<option>${e}</option>`)}
        </select>
        <table width="100%">
            <tr>
                ${this.campos.map(c => html`<th>${c}</th>`)}
            </tr>

            ${this.filas.map(i => html`
                <tr> 
                    ${this.resultados.results[i].valores.map(v => html`<td>${v}</td>`)} 
                </tr>`)}
        
        </table>
        ${this.prev? html`<div><button @click="${this.anterior}">Anterior</button></div>`:html``}
        ${this.sig? html`<div><button @click="${this.siguiente}">Siguiente</button></div>`:html``}

    `;
    }

changeSelect(){
    var entidad = this.shadowRoot.querySelector("#sel").value;
    var url = "http://swapi.dev/api/" + entidad + "/";
    this.cargarDatos(url);
}
    cargarDatos(url) {
        fetch(url).then(response => {
            console.log("response: ", response);
            if (!response.ok) {
                throw response;
            }
            return response.json();
        }).then(data => {
            this.resultados = data;
            this.montarResultados();
            console.log("data ", data);
        }).catch(error => {
            alert("problemas con fetch: ", error);
        })
    }

    /*    connectedCallback() {
            super.connectedCallback();
            try {
                this.cargarPlanetas();
            } catch (e) {
                alert(" error " + e);
            }
        }
    */
    montarResultados() {

        this.campos = this.getObjProps(this.resultados.results[0]);
        this.filas = [];
        for (var i = 0; i < this.resultados.results.length; i++) {
            this.resultados.results[i].valores = this.getVals(this.resultados.results[i]);
            this.filas.push(i);
        }
        this.sig = (this.resultados.next !== null);
        this.prev = (this.resultados.previous!==null);
    }

    getVals(fila) {
        var valores = [];
        for (var i = 0; i < this.campos.length; i++) {
            valores[i] = fila[this.campos[i]];
        }
        return valores;
    }
    getObjProps(obj) {
        var props = [];

        for (var prop in obj) {
            if (!this.isArray(this.resultados.results[0].prop)) {
                if (prop !== "created" && prop !== "edited" && prop !== "url") {
                    props.push(prop);

                }
            }
        }
        return props;
    }
    isArray(prop) {
        return Object.prototype.toString.call(prop) === "[object array]";
    }

    anterior(){
        this.cargarDatos(this.resultados.previous);
    }
    siguiente(){
        this.cargarDatos(this.resultados.next);
    }
}

customElements.define('test-opentable', TestOpentable);